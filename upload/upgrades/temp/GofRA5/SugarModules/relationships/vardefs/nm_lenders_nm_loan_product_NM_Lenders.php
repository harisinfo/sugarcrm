<?php
// created: 2015-12-16 17:04:40
$dictionary["NM_Lenders"]["fields"]["nm_lenders_nm_loan_product"] = array (
  'name' => 'nm_lenders_nm_loan_product',
  'type' => 'link',
  'relationship' => 'nm_lenders_nm_loan_product',
  'source' => 'non-db',
  'module' => 'NM_Loan_Product',
  'bean_name' => 'NM_Loan_Product',
  'side' => 'right',
  'vname' => 'LBL_NM_LENDERS_NM_LOAN_PRODUCT_FROM_NM_LOAN_PRODUCT_TITLE',
);
