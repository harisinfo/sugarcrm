<?php
 // created: 2015-12-16 17:04:40
$layout_defs["NM_Lenders"]["subpanel_setup"]['nm_lenders_nm_loan_product'] = array (
  'order' => 100,
  'module' => 'NM_Loan_Product',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_NM_LENDERS_NM_LOAN_PRODUCT_FROM_NM_LOAN_PRODUCT_TITLE',
  'get_subpanel_data' => 'nm_lenders_nm_loan_product',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
