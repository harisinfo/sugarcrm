<?php
// created: 2015-12-16 17:05:52
$dictionary["NM_Loan_Product"]["fields"]["nm_lenders_nm_loan_product"] = array (
  'name' => 'nm_lenders_nm_loan_product',
  'type' => 'link',
  'relationship' => 'nm_lenders_nm_loan_product',
  'source' => 'non-db',
  'module' => 'NM_Lenders',
  'bean_name' => 'NM_Lenders',
  'vname' => 'LBL_NM_LENDERS_NM_LOAN_PRODUCT_FROM_NM_LENDERS_TITLE',
  'id_name' => 'nm_lenders_nm_loan_productnm_lenders_ida',
);
$dictionary["NM_Loan_Product"]["fields"]["nm_lenders_nm_loan_product_name"] = array (
  'name' => 'nm_lenders_nm_loan_product_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_NM_LENDERS_NM_LOAN_PRODUCT_FROM_NM_LENDERS_TITLE',
  'save' => true,
  'id_name' => 'nm_lenders_nm_loan_productnm_lenders_ida',
  'link' => 'nm_lenders_nm_loan_product',
  'table' => 'nm_lenders',
  'module' => 'NM_Lenders',
  'rname' => 'name',
);
$dictionary["NM_Loan_Product"]["fields"]["nm_lenders_nm_loan_productnm_lenders_ida"] = array (
  'name' => 'nm_lenders_nm_loan_productnm_lenders_ida',
  'type' => 'link',
  'relationship' => 'nm_lenders_nm_loan_product',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_NM_LENDERS_NM_LOAN_PRODUCT_FROM_NM_LOAN_PRODUCT_TITLE',
);
