<?php
// created: 2015-12-17 11:41:35
$dictionary["nm_lenders_nm_loan_product"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'nm_lenders_nm_loan_product' => 
    array (
      'lhs_module' => 'NM_Lenders',
      'lhs_table' => 'nm_lenders',
      'lhs_key' => 'id',
      'rhs_module' => 'NM_Loan_Product',
      'rhs_table' => 'nm_loan_product',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'nm_lenders_nm_loan_product_c',
      'join_key_lhs' => 'nm_lenders_nm_loan_productnm_lenders_ida',
      'join_key_rhs' => 'nm_lenders_nm_loan_productnm_loan_product_idb',
    ),
  ),
  'table' => 'nm_lenders_nm_loan_product_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'nm_lenders_nm_loan_productnm_lenders_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'nm_lenders_nm_loan_productnm_loan_product_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'nm_lenders_nm_loan_productspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'nm_lenders_nm_loan_product_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'nm_lenders_nm_loan_productnm_lenders_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'nm_lenders_nm_loan_product_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'nm_lenders_nm_loan_productnm_loan_product_idb',
      ),
    ),
  ),
);