<?php
// created: 2015-12-17 11:41:35
$dictionary["NM_Lenders"]["fields"]["nm_lenders_contacts"] = array (
  'name' => 'nm_lenders_contacts',
  'type' => 'link',
  'relationship' => 'nm_lenders_contacts',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'side' => 'right',
  'vname' => 'LBL_NM_LENDERS_CONTACTS_FROM_CONTACTS_TITLE',
);
