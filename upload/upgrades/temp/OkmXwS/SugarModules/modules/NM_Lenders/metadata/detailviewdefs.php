<?php
$module_name = 'NM_Lenders';
$viewdefs [$module_name] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'lender_commission',
            'label' => 'LBL_LENDER_COMMISSION',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'lender_address',
            'label' => 'LBL_LENDER_ADDRESS',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'lender_address_city',
            'label' => 'LBL_LENDER_ADDRESS_CITY',
          ),
          1 => 
          array (
            'name' => 'lender_address_country',
            'label' => 'LBL_LENDER_ADDRESS_COUNTRY',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'lender_address_state',
            'label' => 'LBL_LENDER_ADDRESS_STATE',
          ),
          1 => 
          array (
            'name' => 'lender_address_postalcode',
            'label' => 'LBL_LENDER_ADDRESS_POSTALCODE',
          ),
        ),
      ),
    ),
  ),
);
?>
