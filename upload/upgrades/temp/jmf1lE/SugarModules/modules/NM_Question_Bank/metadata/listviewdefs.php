<?php
$module_name = 'NM_Question_Bank';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'QUESTION_GROUP' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'label' => 'LBL_QUESTION_GROUP',
    'id' => 'NM_QUESTION_GROUP_ID_C',
    'link' => true,
    'width' => '10%',
    'default' => true,
  ),
  'QUESTION_TYPE' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_QUESTION_TYPE',
    'width' => '10%',
  ),
);
?>
