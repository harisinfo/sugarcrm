<?php
$module_name = 'NM_Question_Group';
$viewdefs [$module_name] = 
array (
  'QuickCreate' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'group_type',
            'studio' => 'visible',
            'label' => 'LBL_GROUP_TYPE',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'sort_order',
            'studio' => 'visible',
            'label' => 'LBL_SORT_ORDER',
          ),
        ),
      ),
    ),
  ),
);
?>
