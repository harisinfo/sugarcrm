<?php
$module_name = 'NM_product_questions';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'CANNONICAL_QUESTION' => 
  array (
    'type' => 'relate',
    'studio' => 'visible',
    'label' => 'LBL_CANNONICAL_QUESTION',
    'id' => 'NM_QUESTION_BANK_ID_C',
    'link' => true,
    'width' => '10%',
    'default' => true,
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
  ),
);
?>
