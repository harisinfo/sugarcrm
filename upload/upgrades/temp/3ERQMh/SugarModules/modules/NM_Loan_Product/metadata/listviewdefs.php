<?php
$module_name = 'NM_Loan_Product';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'INTEREST_RATE_MIN' => 
  array (
    'type' => 'decimal',
    'label' => 'LBL_INTEREST_RATE_MIN',
    'width' => '10%',
    'default' => true,
  ),
  'INTEREST_RATE_MAX' => 
  array (
    'type' => 'decimal',
    'label' => 'LBL_INTEREST_RATE_MAX',
    'width' => '10%',
    'default' => true,
  ),
  'TERM_IN_MONTHS_MINIMUM' => 
  array (
    'type' => 'int',
    'label' => 'LBL_TERM_IN_MONTHS_MINIMUM',
    'width' => '10%',
    'default' => true,
  ),
  'TERM_IN_MONTHS_MAXIMUM' => 
  array (
    'type' => 'int',
    'label' => 'LBL_TERM_IN_MONTHS_MAXIMUM',
    'width' => '10%',
    'default' => true,
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => false,
  ),
);
?>
