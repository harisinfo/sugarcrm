<?php
$module_name = 'NM_Question_Bank';
$viewdefs [$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'question_group',
            'studio' => 'visible',
            'label' => 'LBL_QUESTION_GROUP',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'question_type',
            'studio' => 'visible',
            'label' => 'LBL_QUESTION_TYPE',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'is_loan_summary_question',
            'label' => 'LBL_IS_LOAN_SUMMARY_QUESTION',
          ),
        ),
        3 => 
        array (
          0 => 'name',
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'question_label',
            'label' => 'LBL_QUESTION_LABEL',
          ),
        ),
      ),
    ),
  ),
);
?>
