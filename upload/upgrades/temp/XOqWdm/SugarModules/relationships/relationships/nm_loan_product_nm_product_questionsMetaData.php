<?php
// created: 2015-12-17 15:38:59
$dictionary["nm_loan_product_nm_product_questions"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'nm_loan_product_nm_product_questions' => 
    array (
      'lhs_module' => 'NM_Loan_Product',
      'lhs_table' => 'nm_loan_product',
      'lhs_key' => 'id',
      'rhs_module' => 'NM_product_questions',
      'rhs_table' => 'nm_product_questions',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'nm_loan_product_nm_product_questions_c',
      'join_key_lhs' => 'nm_loan_product_nm_product_questionsnm_loan_product_ida',
      'join_key_rhs' => 'nm_loan_product_nm_product_questionsnm_product_questions_idb',
    ),
  ),
  'table' => 'nm_loan_product_nm_product_questions_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'nm_loan_product_nm_product_questionsnm_loan_product_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'nm_loan_product_nm_product_questionsnm_product_questions_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'nm_loan_product_nm_product_questionsspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'nm_loan_product_nm_product_questions_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'nm_loan_product_nm_product_questionsnm_loan_product_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'nm_loan_product_nm_product_questions_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'nm_loan_product_nm_product_questionsnm_product_questions_idb',
      ),
    ),
  ),
);