<?php
// created: 2015-12-17 15:38:59
$dictionary["NM_product_questions"]["fields"]["nm_loan_product_nm_product_questions"] = array (
  'name' => 'nm_loan_product_nm_product_questions',
  'type' => 'link',
  'relationship' => 'nm_loan_product_nm_product_questions',
  'source' => 'non-db',
  'module' => 'NM_Loan_Product',
  'bean_name' => 'NM_Loan_Product',
  'vname' => 'LBL_NM_LOAN_PRODUCT_NM_PRODUCT_QUESTIONS_FROM_NM_LOAN_PRODUCT_TITLE',
  'id_name' => 'nm_loan_product_nm_product_questionsnm_loan_product_ida',
);
$dictionary["NM_product_questions"]["fields"]["nm_loan_product_nm_product_questions_name"] = array (
  'name' => 'nm_loan_product_nm_product_questions_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_NM_LOAN_PRODUCT_NM_PRODUCT_QUESTIONS_FROM_NM_LOAN_PRODUCT_TITLE',
  'save' => true,
  'id_name' => 'nm_loan_product_nm_product_questionsnm_loan_product_ida',
  'link' => 'nm_loan_product_nm_product_questions',
  'table' => 'nm_loan_product',
  'module' => 'NM_Loan_Product',
  'rname' => 'name',
);
$dictionary["NM_product_questions"]["fields"]["nm_loan_product_nm_product_questionsnm_loan_product_ida"] = array (
  'name' => 'nm_loan_product_nm_product_questionsnm_loan_product_ida',
  'type' => 'link',
  'relationship' => 'nm_loan_product_nm_product_questions',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_NM_LOAN_PRODUCT_NM_PRODUCT_QUESTIONS_FROM_NM_PRODUCT_QUESTIONS_TITLE',
);
