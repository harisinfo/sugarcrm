<?php
 // created: 2015-12-17 15:38:59
$layout_defs["NM_Loan_Product"]["subpanel_setup"]['nm_loan_product_nm_product_questions'] = array (
  'order' => 100,
  'module' => 'NM_product_questions',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_NM_LOAN_PRODUCT_NM_PRODUCT_QUESTIONS_FROM_NM_PRODUCT_QUESTIONS_TITLE',
  'get_subpanel_data' => 'nm_loan_product_nm_product_questions',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
