<?php
// created: 2016-01-05 06:48:33
$dictionary["Contact"]["fields"]["nm_lenders_contacts"] = array (
  'name' => 'nm_lenders_contacts',
  'type' => 'link',
  'relationship' => 'nm_lenders_contacts',
  'source' => 'non-db',
  'module' => 'NM_Lenders',
  'bean_name' => 'NM_Lenders',
  'vname' => 'LBL_NM_LENDERS_CONTACTS_FROM_NM_LENDERS_TITLE',
  'id_name' => 'nm_lenders_contactsnm_lenders_ida',
);
$dictionary["Contact"]["fields"]["nm_lenders_contacts_name"] = array (
  'name' => 'nm_lenders_contacts_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_NM_LENDERS_CONTACTS_FROM_NM_LENDERS_TITLE',
  'save' => true,
  'id_name' => 'nm_lenders_contactsnm_lenders_ida',
  'link' => 'nm_lenders_contacts',
  'table' => 'nm_lenders',
  'module' => 'NM_Lenders',
  'rname' => 'name',
);
$dictionary["Contact"]["fields"]["nm_lenders_contactsnm_lenders_ida"] = array (
  'name' => 'nm_lenders_contactsnm_lenders_ida',
  'type' => 'link',
  'relationship' => 'nm_lenders_contacts',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_NM_LENDERS_CONTACTS_FROM_CONTACTS_TITLE',
);
