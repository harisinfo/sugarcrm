<?php
// created: 2016-01-05 06:47:10
$dictionary["NM_Loan_Product"]["fields"]["nm_loan_product_nm_product_questions"] = array (
  'name' => 'nm_loan_product_nm_product_questions',
  'type' => 'link',
  'relationship' => 'nm_loan_product_nm_product_questions',
  'source' => 'non-db',
  'module' => 'NM_product_questions',
  'bean_name' => 'NM_product_questions',
  'side' => 'right',
  'vname' => 'LBL_NM_LOAN_PRODUCT_NM_PRODUCT_QUESTIONS_FROM_NM_PRODUCT_QUESTIONS_TITLE',
);
