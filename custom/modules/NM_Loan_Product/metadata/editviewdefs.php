<?php
$module_name = 'NM_Loan_Product';
$viewdefs [$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
        ),
        1 => 
        array (
          0 => 'description',
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'product_fee_type',
            'studio' => 'visible',
            'label' => 'LBL_PRODUCT_FEE_TYPE',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'interest_rate_min',
            'label' => 'LBL_INTEREST_RATE_MIN',
          ),
          1 => 
          array (
            'name' => 'interest_rate_max',
            'label' => 'LBL_INTEREST_RATE_MAX',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'term_in_months_minimum',
            'label' => 'LBL_TERM_IN_MONTHS_MINIMUM',
          ),
          1 => 
          array (
            'name' => 'term_in_months_maximum',
            'label' => 'LBL_TERM_IN_MONTHS_MAXIMUM',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'fee_percent',
            'label' => 'LBL_FEE_PERCENT',
          ),
          1 => 
          array (
            'name' => 'capping_limit_fee',
            'label' => 'LBL_CAPPING_LIMIT_FEE',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'fixed_fee',
            'label' => 'LBL_FIXED_FEE',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'ltv_min_c',
            'label' => 'LBL_LTV_MIN',
          ),
          1 => 
          array (
            'name' => 'ltv_max_c',
            'label' => 'LBL_LTV_MAX',
          ),
        ),
        8 => 
        array (
          0 => 
          array (
            'name' => 'loan_amount_min_c',
            'label' => 'LBL_LOAN_AMOUNT_MIN',
          ),
          1 => 
          array (
            'name' => 'loan_amount_max_c',
            'label' => 'LBL_LOAN_AMOUNT_MAX',
          ),
        ),
        9 => 
        array (
          0 => 
          array (
            'name' => 'is_active',
            'label' => 'LBL_IS_ACTIVE',
          ),
          1 => 
          array (
            'name' => 'nm_lenders_nm_loan_product_name',
          ),
        ),
      ),
    ),
  ),
);
?>
