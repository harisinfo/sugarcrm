<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2016-01-05 06:48:33
$dictionary["NM_Loan_Product"]["fields"]["nm_lenders_nm_loan_product"] = array (
  'name' => 'nm_lenders_nm_loan_product',
  'type' => 'link',
  'relationship' => 'nm_lenders_nm_loan_product',
  'source' => 'non-db',
  'module' => 'NM_Lenders',
  'bean_name' => 'NM_Lenders',
  'vname' => 'LBL_NM_LENDERS_NM_LOAN_PRODUCT_FROM_NM_LENDERS_TITLE',
  'id_name' => 'nm_lenders_nm_loan_productnm_lenders_ida',
);
$dictionary["NM_Loan_Product"]["fields"]["nm_lenders_nm_loan_product_name"] = array (
  'name' => 'nm_lenders_nm_loan_product_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_NM_LENDERS_NM_LOAN_PRODUCT_FROM_NM_LENDERS_TITLE',
  'save' => true,
  'id_name' => 'nm_lenders_nm_loan_productnm_lenders_ida',
  'link' => 'nm_lenders_nm_loan_product',
  'table' => 'nm_lenders',
  'module' => 'NM_Lenders',
  'rname' => 'name',
);
$dictionary["NM_Loan_Product"]["fields"]["nm_lenders_nm_loan_productnm_lenders_ida"] = array (
  'name' => 'nm_lenders_nm_loan_productnm_lenders_ida',
  'type' => 'link',
  'relationship' => 'nm_lenders_nm_loan_product',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_NM_LENDERS_NM_LOAN_PRODUCT_FROM_NM_LOAN_PRODUCT_TITLE',
);


// created: 2016-01-05 06:47:10
$dictionary["NM_Loan_Product"]["fields"]["nm_loan_product_nm_product_questions"] = array (
  'name' => 'nm_loan_product_nm_product_questions',
  'type' => 'link',
  'relationship' => 'nm_loan_product_nm_product_questions',
  'source' => 'non-db',
  'module' => 'NM_product_questions',
  'bean_name' => 'NM_product_questions',
  'side' => 'right',
  'vname' => 'LBL_NM_LOAN_PRODUCT_NM_PRODUCT_QUESTIONS_FROM_NM_PRODUCT_QUESTIONS_TITLE',
);


 // created: 2016-01-06 09:15:24
$dictionary['NM_Loan_Product']['fields']['loan_amount_min_c']['labelValue']='Loan Amount Min';

 

 // created: 2016-01-06 09:15:35
$dictionary['NM_Loan_Product']['fields']['loan_amount_max_c']['labelValue']='Loan amount max';

 

 // created: 2016-01-05 10:25:34
$dictionary['NM_Loan_Product']['fields']['ltv_min_c']['labelValue']='LTV min';

 

 // created: 2016-01-05 10:25:58
$dictionary['NM_Loan_Product']['fields']['ltv_max_c']['labelValue']='LTV max';

 
?>