<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2016-01-05 06:48:33
$dictionary["NM_Lenders"]["fields"]["nm_lenders_contacts"] = array (
  'name' => 'nm_lenders_contacts',
  'type' => 'link',
  'relationship' => 'nm_lenders_contacts',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'side' => 'right',
  'vname' => 'LBL_NM_LENDERS_CONTACTS_FROM_CONTACTS_TITLE',
);


// created: 2016-01-05 06:48:33
$dictionary["NM_Lenders"]["fields"]["nm_lenders_nm_loan_product"] = array (
  'name' => 'nm_lenders_nm_loan_product',
  'type' => 'link',
  'relationship' => 'nm_lenders_nm_loan_product',
  'source' => 'non-db',
  'module' => 'NM_Loan_Product',
  'bean_name' => 'NM_Loan_Product',
  'side' => 'right',
  'vname' => 'LBL_NM_LENDERS_NM_LOAN_PRODUCT_FROM_NM_LOAN_PRODUCT_TITLE',
);

?>