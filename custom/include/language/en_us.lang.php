<?php
$app_strings['LBL_GROUPTAB5_1450190331'] = 'Manage Lenders';

$GLOBALS['app_list_strings']['product_fee_type']=array (
  'percentage' => '%',
  'fixed_value' => 'Fixed value',
);
$GLOBALS['app_list_strings']['question_groups']=array (
  'group_1' => 'Group 1',
  'group_2' => 'Group 2',
  'group_3' => 'Group 3',
);
$GLOBALS['app_list_strings']['group_type']=array (
  'personal' => 'Personal',
  'corporate' => 'Corporate',
  'product' => 'Product',
);
$GLOBALS['app_list_strings']['question_type']=array (
  'number' => 'Number',
  'date' => 'Date',
  'single_select' => 'Single Select',
  'multi_select' => 'Multi Select',
  'file_upload' => 'File Upload',
  'text' => 'text',
  'textarea' => 'textarea',
);
$GLOBALS['app_list_strings']['question_order']=array (
  1 => '1',
);
$GLOBALS['app_list_strings']['group_order']=array (
  1 => '1',
  2 => '2',
  3 => '3',
  4 => '4',
  5 => '5',
  6 => '6',
  7 => '7',
  8 => '8',
  9 => '9',
  10 => '10',
  11 => '11',
  12 => '12',
  13 => '13',
  14 => '14',
  15 => '15',
  16 => '16',
  17 => '17',
  18 => '18',
);