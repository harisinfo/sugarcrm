<?php
$module_name = 'NM_Question_Bank';
$viewdefs [$module_name] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'question_label',
            'label' => 'LBL_QUESTION_LABEL',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'question_group',
            'studio' => 'visible',
            'label' => 'LBL_QUESTION_GROUP',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'question_type',
            'studio' => 'visible',
            'label' => 'LBL_QUESTION_TYPE',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'min_date',
            'label' => 'LBL_MIN_DATE',
          ),
          1 => 
          array (
            'name' => 'max_date',
            'label' => 'LBL_MAX_DATE',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'min_value',
            'label' => 'LBL_MIN_VALUE',
          ),
          1 => 
          array (
            'name' => 'max_value',
            'label' => 'LBL_MAX_VALUE',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'is_loan_summary_question',
            'label' => 'LBL_IS_LOAN_SUMMARY_QUESTION',
          ),
        ),
      ),
    ),
  ),
);
?>
