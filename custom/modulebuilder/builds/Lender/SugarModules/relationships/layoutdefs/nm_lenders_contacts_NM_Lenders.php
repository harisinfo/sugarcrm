<?php
 // created: 2016-01-05 06:48:33
$layout_defs["NM_Lenders"]["subpanel_setup"]['nm_lenders_contacts'] = array (
  'order' => 100,
  'module' => 'Contacts',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_NM_LENDERS_CONTACTS_FROM_CONTACTS_TITLE',
  'get_subpanel_data' => 'nm_lenders_contacts',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
