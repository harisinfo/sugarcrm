<?php
$module_name = 'NM_Lenders';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'LENDER_COMMISSION' => 
  array (
    'type' => 'decimal',
    'default' => true,
    'label' => 'LBL_LENDER_COMMISSION',
    'width' => '10%',
  ),
  'LENDER_ADDRESS' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_LENDER_ADDRESS',
    'width' => '10%',
    'default' => true,
  ),
  'LENDER_ADDRESS_CITY' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_LENDER_ADDRESS_CITY',
    'width' => '10%',
    'default' => true,
  ),
  'LENDER_ADDRESS_POSTALCODE' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_LENDER_ADDRESS_POSTALCODE',
    'width' => '10%',
    'default' => true,
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
  ),
);
?>
