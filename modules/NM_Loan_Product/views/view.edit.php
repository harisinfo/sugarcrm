<?php

class NM_Loan_ProductViewEdit extends ViewEdit
{
	function display()
	{
		//global $mod_strings;
		//parent::display();
		echo $this->check_product_fee_status_js();
		echo "hello world";
		parent::display();
	}


	function check_product_fee_status_js()
	{
		global $mod_strings;

		$jsscript = "
                   <script type='text/javascript'>
 
		$( document ).ready(function() {                	

		$('#product_fee_type').change(function() {
                	makerequired(); // onchange call function to mark the field required
               	});

		function makerequired()
              	{
              		var status = $('#product_fee_type').val(); // get current value of the field
			console.log( status );
			resetrequired();
			if(status == 'fixed_value')
			{ 
                       		addToValidate('EditView','fixed_fee','varchar',true,'{$mod_strings['LBL_FIXED_FEE']}');    
                          	$('#fixed_fee_label').html('{$mod_strings['LBL_FIXED_FEE']}: <font color=\"red\">*</font>'); 
                       	}

			if(status == 'percentage')
                        {
                                addToValidate('EditView','fee_percent','varchar',true,'{$mod_strings['LBL_FEE_PERCENT']}');
                                $('#fee_percent_label').html('{$mod_strings['LBL_FEE_PERCENT']}: <font color=\"red\">*</font>');
                        	
				// capping limit
				addToValidate( 'EditView', 'capping_limit_fee', 'varchar',true, '{$mod_strings[ 'LBL_CAPPING_LIMIT_FEE' ]}' );
				$('#capping_limit_fee_label').html('{$mod_strings['LBL_CAPPING_LIMIT_FEE']}: <font color=\"red\">*</font>');
			}
		}

		function resetrequired()
		{
			removeFromValidate('EditView','fee_percent');
			 $('#fee_percent_label').html('{$mod_strings['LBL_FEE_PERCENT']}: ');

			removeFromValidate('EditView','fixed_fee');
			$('#fixed_fee_label').html('{$mod_strings['LBL_FIXED_FEE']}: ');		
		
			removeFromValidate('EditView','capping_limit_fee');
                        $('#capping_limit_fee_label').html('{$mod_strings['LBL_CAPPING_LIMIT_FEE']}: ');
		}

		//resetrequired();
		makerequired();
		});
		</script>
		";

		return $jsscript;
	}
}
